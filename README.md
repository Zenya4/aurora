![Banner](https://imgur.com/yqujPbM.png)
#### Illuminate the night with breathtaking ambient particle effects

## Overview
Aurora provides a unique experience for all players by generating dynamic **ambient particle effects** using a **high-performance** particle engine. All particles are **fully configurable** and can be **customised per-biome** to fit each server's needs. The base plugin comes with multiple **high-quality default configs** covering all particle shapes Aurora has to offer, available for almost all vanilla biomes.

## Wiki
#### Aurora
- [Home](https://gitlab.com/Zenya4/aurora/-/wikis/Home)
- [Features](https://gitlab.com/Zenya4/aurora/-/wikis/Features)
- [Commands](https://gitlab.com/Zenya4/aurora/-/wikis/Commands)
- [Permissions](https://gitlab.com/Zenya4/aurora/-/wikis/Permissions)

#### Configuration
- [General Configuration](https://gitlab.com/Zenya4/aurora/-/wikis/General-Configuration)
- [Particle Configuration](https://gitlab.com/Zenya4/aurora/-/wikis/Particle-Configuration)
- [Biome Presents](https://gitlab.com/Zenya4/aurora/-/wikis/Biome-Presents)

#### Support
- [Frequently Asked Questions](https://gitlab.com/Zenya4/aurora/-/wikis/Frequently-Asked-Questions)
- [WorldGuard Compatibility](https://gitlab.com/Zenya4/aurora/-/wikis/WorldGuard-Compatibility)
- [Developer API](https://gitlab.com/Zenya4/aurora/-/wikis/Developer-API)
- [Terms Of Service](https://gitlab.com/Zenya4/aurora/-/wikis/Terms-Of-Service)

## More Info
[Download](https://www.spigotmc.org/resources/%E2%98%84%EF%B8%8Faurora%E2%98%84%EF%B8%8F-ambient-particle-display-customisable-per-biome.89399/) (SpigotMC)<br>
[Support](https://discord.gg/KGuaxpM) (Discord)